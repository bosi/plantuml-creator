package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"strings"
)

type Diagram struct {
	Title  string
	ImgUrl string
}

func renderTemplate(w http.ResponseWriter, view string, p interface{}) {
	err := templates.ExecuteTemplate(w, view+".html", p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	data := struct {
		Diagrams []Diagram
	}{}

	files, _ := ioutil.ReadDir(dataDir)

	for _, file := range files {
		if file.Name() == ".gitignore" {
			continue
		}

		title := strings.Split(file.Name(), ".")[0]

		diagram := Diagram{
			Title:  title,
			ImgUrl: imageDir + "/" + title + ".png",
		}

		data.Diagrams = append(data.Diagrams, diagram)
	}

	renderTemplate(w, "index", data)
}

func createHandler(w http.ResponseWriter, r *http.Request) {
	title := r.FormValue("title")
	p, err := loadPage(title)
	if err != nil {
		p = &Page{Title: title}
	}
	renderTemplate(w, "edit", p)
}

func viewHandler(w http.ResponseWriter, r *http.Request, title string) {
	p, err := loadPage(title)
	if err != nil {
		http.Redirect(w, r, "/edit/"+title, http.StatusFound)
		return
	}
	renderTemplate(w, "view", p)
}



func editHandler(w http.ResponseWriter, r *http.Request, title string) {
	p, err := loadPage(title)
	if err != nil {
		p = &Page{Title: title}
	}
	renderTemplate(w, "edit", p)
}

func saveHandler(w http.ResponseWriter, r *http.Request, title string) {
	body := r.FormValue("body")
	p := &Page{Title: title, Body: []byte(body)}
	err := p.save()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	filename := "/app/" + dataDir + "/" + p.Title + ".txt"
	cmd := exec.Command("/usr/bin/java", "-jar", "/app/plantuml.jar", filename, "-o", "/app/"+imageDir)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err = cmd.Run()
	if err != nil {
		fmt.Println(err)
	}

	http.Redirect(w, r, "/view/"+title, http.StatusFound)
}

func makeHandler(fn func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		m := validPath.FindStringSubmatch(r.URL.Path)
		if m == nil {
			http.NotFound(w, r)
			return
		}
		fn(w, r, m[2])
	}
}

package main

import (
	"html/template"
	"log"
	"net/http"
	"regexp"
)

const (
	viewDir  = "./views"
	dataDir  = "./data/raw"
	imageDir = "./data/images"
)

var templates *template.Template

func main() {
	loadViews([]string{"index", "view", "edit"})

	fs := http.FileServer(http.Dir(imageDir))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	loadRoutes(map[string]func(http.ResponseWriter, *http.Request){
		"/":       indexHandler,
		"/create": createHandler,
		"/view/":  makeHandler(viewHandler),
		"/edit/":  makeHandler(editHandler),
		"/save/":  makeHandler(saveHandler),
	})

	log.Fatal(http.ListenAndServe(":1234", nil))
}

func loadRoutes(routeMapping map[string]func(http.ResponseWriter, *http.Request)) {
	for pattern, handler := range routeMapping {
		http.HandleFunc(pattern, handler)
	}
}

func loadViews(views []string) {
	viewPaths := []string{}
	for _, view := range views {
		viewPaths = append(viewPaths, viewDir+"/"+view+".html")
	}
	templates = template.Must(template.ParseFiles(viewPaths...))
}

var validPath = regexp.MustCompile("^/(edit|save|view)/([a-zA-Z0-9]+)$")

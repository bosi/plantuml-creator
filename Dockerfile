############################
# STEP 1 build executable binary
############################
FROM golang:1.12 AS builder

WORKDIR $GOPATH/src/plantuml-creator

RUN apt-get install git

COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -v

############################
# STEP 2 build a small image
############################
FROM frolvlad/alpine-java

WORKDIR /app

ENV PLANTUML_VERSION=1.2019.3

RUN \
  apk add --no-cache graphviz wget ca-certificates && \
  wget "http://downloads.sourceforge.net/project/plantuml/${PLANTUML_VERSION}/plantuml.${PLANTUML_VERSION}.jar" -O plantuml.jar && \
  apk del wget ca-certificates

ENV LANG en_US.UTF-8

COPY --from=builder /go/src/plantuml-creator/plantuml-creator /app/
COPY . /app/

EXPOSE 1234

ENTRYPOINT ["./plantuml-creator"]

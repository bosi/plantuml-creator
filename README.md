# Plantuml-Creator
I am great at thinking up names. That's why this package is called plantuml-creator...it is a web interface for creating and saving diagrams with plantuml.

## Usage
The best way to use the plantuml-creator is by using docker-compose.yml:
```yaml
version: "3"

services:
    plantuml-creator:
        image: bosix/plantuml-creator
        restart: always
        ports:
          - 1234:1234
        volumes:
            - ./data:/app/data
```
```
docker-compose up -d
```

You can found your plantuml sourcecode and the created images in /app/data. The app will listen on port 1234